const nodemailer = require("nodemailer");
const moment = require('moment');
const hbs = require('nodemailer-express-handlebars')
const path = require("path");


moment.locale('fr');

const transport = nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
        user: process.env.MAILER_USER,
        pass: process.env.MAILER_PASS
    }
});

// point to the template folder
const handlebarOptions = {
    viewEngine: {
        partialsDir: path.resolve('./views/'),
        defaultLayout: false,
    },
    viewPath: path.resolve('./views/'),
};

// use a template file with nodemailer
transport.use('compile', hbs(handlebarOptions))

module.exports.sendConfirmationEmail = (data, email) => {
    data = JSON.parse(JSON.stringify(data));
    data.forEach((elem) => {
        elem.product.TTC =  Number(elem.product.price * (1 + elem.product.tva / 100)).toFixed(2);
        elem.TTC = Number((elem.product.price * (1 + elem.product.tva / 100)) * elem.quantity).toFixed(2);
    })
    data[0].order.soustotal = (data[0].order.total - 1).toFixed(2)
    const mailOptions = {
        from: 'commandes@littledonuts.com', // sender address
        to: email, // list of receivers
        subject: "Une nouvelle commande est arrivée !",
        template: 'email', // the name of the template file ie email.handlebars
        context:{
            order: data[0].order,
            date: moment(data[0].order.createdAt).format('LLL'),
            products: data
        }
    };
    // trigger the sending of the E-mail
    transport.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
};
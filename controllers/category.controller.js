const db = require("../models");
const Category = db.category;

exports.create = (req, res) => {
    // Validate Request
    if (!req.body.name || !req.body.description || !req.body.order) {
        res.status(400).send({
            message: "Name , description and order is required !"
        });
        return;
    }

    // Create a Category
    const category = {
        name: req.body.name,
        description: req.body.description,
        order: req.body.order
    };

    // Save Category in the database
    Category.create(category)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured while creating the Category."
            });
        });
}

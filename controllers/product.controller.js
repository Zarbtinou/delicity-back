const db = require("../models");
const Product = db.product;
const Category = db.category;

exports.create = (req, res) => {
    // Validate Request
    if (
        !req.body.name || !req.body.price ||
        !req.body.tva || !req.body.order ||
        !req.body.category || !req.body.picture
    ) {
        res.status(400).send({
            message: "Name, price, tva, order, category and picture is required !"
        });
        return;
    }

    // Create a Product
    const product = {
        name: req.body.name,
        price: req.body.price,
        tva: req.body.tva,
        order: req.body.order,
        categoryId: req.body.category,
        picture: req.body.picture
    };

    // Save Product in the database
    Product.create(product)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured while creating the Product."
            });
        });
};

exports.findAll= (req, res) => {
    let categories = null
    // Retrieve all categories
    Category.findAll({
        order: [['order', 'ASC']]
    }).then(response => {
           categories = response
        }
    ).catch(err => {
        res.status(500).send({
            message:
                err.message || "Some error occured while retrieving the Categories."
        });
    });

    // Retrieve all Products
    Product.findAll({
        // Attributes we want to retrieve
        attributes: ['id', 'name', 'price', 'tva', 'order', 'picture'],
        // Retrieve associated Category
        include: Category,
        // Sort by Category order first, then product order
        order: [
            [Category, 'order', 'ASC'],
            ['order', 'ASC']
        ]
    })
        .then(data => {
            res.send({"products": data, "categories": categories});
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured while retrieving the Products."
            });
        });
};
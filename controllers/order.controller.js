const db = require("../models");
const nodemailer = require("../config/nodemailer.config");
const OrderItem = db.orderItem;
const Order = db.order;
const Product = db.product;

exports.create = (req, res) => {
    let order = null
    let orderInfos = null

    Order.create({
        total: req.body.total,

    }).then( async data => {
        order = data.id;
        const x = async () => {req.body.products.forEach((product) => {
            OrderItem.create({
                productId: product.id,
                orderId: data.id,
                quantity: product.quantity
            })
        })}
        await x()
        await new Promise(r => setTimeout(r, 80));
        orderInfos = await OrderItem.findAll({
            include: [Product, Order],
            where: {
                orderId: order
            }
        });
        if (orderInfos.length < req.body.products.length) {
            throw {'error': 'An error has occured'}
        }
        nodemailer.sendConfirmationEmail(
            orderInfos,
            'contact@delicity.com'
        );
        res.send({message: orderInfos});
    })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured while creating the Order."
            });
        });
}
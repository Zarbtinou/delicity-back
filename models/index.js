const Sequelize = require("sequelize");
const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    dialect: 'mariadb',
    operatorsAliases: false,

});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.category = require("./category.model.js")(sequelize, Sequelize);
db.product = require("./product.model.js")(sequelize, Sequelize);
db.order = require("./order.model.js")(sequelize, Sequelize);
db.orderItem = require("./orderitem.model")(sequelize, Sequelize);


// Relations

// OneToMany relation between Product and Category
db.product.belongsTo(db.category)
db.category.hasMany(db.product)

// OneToMany relation between OrderItem and Product
db.orderItem.belongsTo(db.product)
db.product.hasMany(db.orderItem)

// OneToMany relation between OrderItem and Order
db.orderItem.belongsTo(db.order)
db.order.hasMany(db.orderItem)

module.exports = db;
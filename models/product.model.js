module.exports = (sequelize, Sequelize) => {
    return sequelize.define("product", {
    name: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.FLOAT
    },
    tva: {
        type: Sequelize.FLOAT
    },
    order: {
        type: Sequelize.INTEGER
    },
    picture: {
        type: Sequelize.STRING
    },
});
};
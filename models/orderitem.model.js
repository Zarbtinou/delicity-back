module.exports = (sequelize, Sequelize) => {
    return sequelize.define("orderitem", {
        quantity: {
            type: Sequelize.INTEGER
        }
    });
};
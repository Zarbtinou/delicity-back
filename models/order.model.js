module.exports = (sequelize, Sequelize) => {
    return sequelize.define("order", {
        total: {
            type: Sequelize.FLOAT
        }
    });
};
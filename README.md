# delicity-back

Technical test for delicity back-end application.

## Project Setup

```sh
npm install
```
Create your .env files based on the .env.example and fill it with your own variables

## Run project

```sh
node app.js
```

## Database example

Under the directory sql databasess, there is an example of the db used during the developpement, you can import it to test the application more faster. There is a second export in format utf8mb4_general_ci for production tests

## Example mailer

There is a public mailtrap test account for check confirm order email :

url: https://mailtrap.io/

user: qdr.moreau@yahoo.fr

pass: Delicity06000

Go Email API and sign up to access the mail inboxe
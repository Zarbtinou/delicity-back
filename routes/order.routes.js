module.exports = app => {
    const order = require("../controllers/order.controller");

    const router = require("express").Router();

    // Create a new Order
    router.post("/", order.create);

    app.use('/api/order', router);
};
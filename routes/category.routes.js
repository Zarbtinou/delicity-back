module.exports = app => {
    const category = require("../controllers/category.controller.js");

    const router = require("express").Router();

    // Create a new Category
    router.post("/", category.create);

    app.use('/api/category', router);
};
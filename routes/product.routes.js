module.exports = app => {
    const product = require("../controllers/product.controller.js");

    const router = require("express").Router();

    // Create a new Product
    router.post("/", product.create);

    // Retrieve all product sorted by product order and category order
    router.get("/", product.findAll);

    app.use('/api/product', router);
};
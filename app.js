const express = require('express')
const app = express()
const cors = require('cors')
const port = 3005

// parse requests of content-type - application/json
app.use(express.json());

// CORS is enabled for all origins
app.use(cors());

// Enable ENV vars
require('dotenv').config();

// Create DB connection and insert Tables if not exists
const db = require("./models");
db.sequelize.sync()
    .then(() => {
        console.log("Drop and re-sync db.");
    })
    .catch((err) => {
        console.log("Failed to sync db: " + err.message);
    });

// Add routes
require("./routes/category.routes.js")(app);
require("./routes/product.routes.js")(app);
require("./routes/order.routes.js")(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})